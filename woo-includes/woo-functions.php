<?php
defined( 'ABSPATH' ) || exit;

/**
 * Functions used by plugins
 */
if ( ! class_exists( 'WCST_Dependencies' ) ) {
	require_once 'class-wc-dependencies.php';
}

/**
 * WC Detection
 */
if ( ! function_exists( 'xl_wcst_is_woocommerce_active' ) ) {

	function xl_wcst_is_woocommerce_active() {
		return WCST_Dependencies::woocommerce_active_check();
	}
}
