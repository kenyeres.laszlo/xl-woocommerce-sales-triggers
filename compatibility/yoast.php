<?php
defined( 'ABSPATH' ) || exit;

class WCST_Compatibility_YOAST {

	public function __construct() {
		add_filter( 'wcst_best_seller_badge_cat_id', array( $this, 'setup_primary_cat_badge' ), 99, 2 );
		add_filter( 'wcst_static_badge_cat_id', array( $this, 'setup_primary_cat_badge' ), 99, 2 );
	}

	/**
	 * Check if yoast installed and product has primary category
	 *
	 * @param $term_id
	 * @param $product_info
	 *
	 * @return int
	 */
	public function setup_primary_cat_badge( $term_id, $product_info ) {
		if ( defined( 'YOAST_ENVIRONMENT' ) ) {
			$product_meta = WCST_Common::get_post_meta_data( $product_info->product->get_id() );
			$primary_cat  = isset( $product_meta['_yoast_wpseo_primary_product_cat'] ) ? $product_meta['_yoast_wpseo_primary_product_cat'] : 0;
			if ( absint( $primary_cat ) > 0 ) {
				$term_id = absint( $primary_cat );
			}
		}

		return $term_id;
	}
}

new WCST_Compatibility_YOAST();
