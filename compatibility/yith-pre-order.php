<?php
defined( 'ABSPATH' ) || exit;

class WCST_Compatibility_YITH_Pre_Order {

	public function __construct() {
		add_filter( 'wp_loaded', array( $this, 'show_product_stock_html' ), 99 );
	}

	/**
	 * Check if yith woocommerce pre order plugin installed
	 * remove sale trigger hide stock html code
	 */
	public function show_product_stock_html() {
		if ( function_exists( 'yith_ywpo_init' ) ) {
			add_filter( 'wcst_product_stock_html_hide', function () {
				return false;
			} );
		}
	}
}

new WCST_Compatibility_YITH_Pre_Order();
