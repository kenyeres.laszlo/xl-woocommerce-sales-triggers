<?php
defined( 'ABSPATH' ) || exit;

class WCST_Trigger_Dynamic_Number extends WCST_Base_Trigger {
	public $slug = 'dynamic_number';
	public $parent_slug = 'wcst_dynamic_number_settings';
	public $title = '';
	public $default_priority = 9;

	public function get_defaults() {
		return array(
			'box_bg_color'     => '#efeddc',
			'border_color'     => '#efeace',
			'range_from'       => 0,
			'range_to'         => 10,
			'hold_value'       => 0,
			'hold_value_level' => 'website',
			'text'             => array(
				0 => array(
					'text'      => __( '{{dynamic_number}} people purchased this product in last 30 minutes', WCST_SLUG ),
					'condition' => 1,
					'number'    => 5,
				),
				1 => array(
					'text'      => __( '{{dynamic_number}} people purchased this product in last 10 minutes', WCST_SLUG ),
					'condition' => 2,
					'number'    => 5,
				),
			),
			'text_color'       => '#252525',
			'active_color'     => '#dd3333',
			'font_size'        => 16,
			'position'         => 5,
		);
	}

	public function register_settings() {
		$this->settings = array(
			array(
				'id'         => '_wcst_data_wcst_dynamic_number_html',
				'type'       => 'wcst_html_content_field',
				'content'    => '<div class="wcst_desc_before_row">' . __( 'You can display random numbers on website to boost your sales.', WCST_SLUG ) . '</div>',
				'attributes' => array(
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
			array(
				'name'        => __( 'Dynamic Number Range', WCST_SLUG ),
				'id'          => '_wcst_data_wcst_dynamic_number_range_from',
				'type'        => 'text',
				'row_classes' => array( 'wcst_combine_2_field_small_start' ),
				'before'      => __( 'From', WCST_SLUG ),
				'attributes'  => array(
					'placeholder'            => '0',
					'type'                   => 'number',
					'min'                    => '0',
					'pattern'                => '\d*',
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
			array(
				'name'        => __( 'To', WCST_SLUG ),
				'id'          => '_wcst_data_wcst_dynamic_number_range_to',
				'type'        => 'text',
				'row_classes' => array( 'wcst_combine_2_field_small_end', 'wcst_hide_label' ),
				'before'      => __( 'To', WCST_SLUG ),
				'attributes'  => array(
					'placeholder'            => '10',
					'type'                   => 'number',
					'min'                    => '0',
					'pattern'                => '\d*',
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
			array(
				'id'          => 'wcst_dynamic_number_html_range',
				'type'        => 'wcst_html_content_field',
				'row_classes' => array( 'wcst_clear_both wcst_label_gap cmb2-wcst_mb15' ),
				'content'     => __( 'Dynamic number will auto pick from above range.', WCST_SLUG ),
				'attributes'  => array(
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
			array(
				'name'        => __( 'Hold Value For', WCST_SLUG ),
				'desc'        => __( 'Seconds', WCST_SLUG ),
				'id'          => '_wcst_data_wcst_dynamic_number_hold_value',
				'row_classes' => array( 'wcst_field_inline_desc wcst_clear_both' ),
				'type'        => 'text',
				'attributes'  => array(
					'type'                   => 'number',
					'min'                    => '0',
					'pattern'                => '\d*',
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
			array(
				'id'          => 'wcst_dynamic_number_html_hold_val',
				'type'        => 'wcst_html_content_field',
				'row_classes' => array( 'wcst_clear_both wcst_label_gap cmb2-wcst_mb15' ),
				'content'     => __( 'Dynamic number value held for xx seconds using cookies. Leave 0 for auto change on every refresh.', WCST_SLUG ),
				'attributes'  => array(
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
			array(
				'name'       => __( 'Hold Value', WCST_SLUG ),
				'id'         => '_wcst_data_wcst_dynamic_number_hold_value_level',
				'type'       => 'radio_inline',
				'options'    => array(
					'website' => __( 'Site Wide', WCST_SLUG ),
					'product' => __( 'Product Wise', WCST_SLUG ),
				),
				'desc'       => __( 'Site wide: This holds dynamic number value for complete site like xx people are online or so.<br/>Product wise: This holds dynamic number value specific to the product like xx people have added to cart or so.', WCST_SLUG ),
				'attributes' => array(
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
			/* ************** START REPEATER ************** */
			array(
				'id'           => '_wcst_data_wcst_dynamic_number_text',
				'type'         => 'group',
				'before_group' => array( 'WCST_Admin_CMB2_Support', 'cmb2_wcst_before_dynamic_number_call' ),
				'after_group'  => array( 'WCST_Admin_CMB2_Support', 'cmb2_wcst_after_dynamic_number_call' ),
				'repeatable'   => true, // use false if you want non-repeatable group
				'attributes'   => array(
					'class'                  => 'wcst_dynamic_number_group wcst_repeated_group',
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
				'options'      => array(
					'group_title'   => __( 'Text', WCST_SLUG ), // since version 1.1.4, {#} gets replaced by row number
					'add_button'    => __( 'Add Text', WCST_SLUG ),
					'remove_button' => __( 'Remove Text', WCST_SLUG ),
					'sortable'      => true, // true to enable sorting
					'closed'        => true, // true to have the groups closed by default
				),
				'fields'       => array(
					array(
						'name'       => __( 'Text', WCST_SLUG ),
						'id'         => 'text',
						'type'       => 'textarea',
						'desc'       => '<i>{{dynamic_number}}</i> displays dynamic number.',
						'attributes' => array(
							'rows' => '4',
						),
					),
					array(
						'name'    => __( 'Select Condition', WCST_SLUG ),
						'id'      => 'condition',
						'type'    => 'select',
						'options' => array(
							'1' => 'Greater Than',
							'2' => 'Less Than',
						),
					),
					array(
						'name'       => __( 'Number', WCST_SLUG ),
						'id'         => 'number',
						'type'       => 'text',
						'attributes' => array(
							'type'    => 'number',
							'min'     => '0',
							'pattern' => '\d*',
						),
					),
				),
			),
			/* ****************** END REPEATER ****************** */
			array(
				'name'       => __( 'Background Color', WCST_SLUG ),
				'id'         => '_wcst_data_wcst_dynamic_number_box_bg_color',
				'type'       => 'colorpicker',
				'attributes' => array(
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
			array(
				'name'       => __( 'Border Color', WCST_SLUG ),
				'id'         => '_wcst_data_wcst_dynamic_number_border_color',
				'type'       => 'colorpicker',
				'attributes' => array(
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
			array(
				'name'       => __( 'Text Color', WCST_SLUG ),
				'id'         => '_wcst_data_wcst_dynamic_number_text_color',
				'type'       => 'colorpicker',
				'attributes' => array(
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
			array(
				'name'       => __( 'Active Number Color', WCST_SLUG ),
				'id'         => '_wcst_data_wcst_dynamic_number_active_color',
				'type'       => 'colorpicker',
				'attributes' => array(
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
			array(
				'name'        => __( 'Font Size', WCST_SLUG ),
				'desc'        => __( 'px', WCST_SLUG ),
				'id'          => '_wcst_data_wcst_dynamic_number_font_size',
				'row_classes' => array( 'wcst_field_inline_desc' ),
				'type'        => 'text',
				'attributes'  => array(
					'type'                   => 'number',
					'min'                    => '0',
					'pattern'                => '\d*',
					'data-conditional-id'    => '_wcst_data_choose_trigger',
					'data-conditional-value' => 'dynamic_number',
				),
			),
		);
	}

	public function get_post_settings() {
		return array(
			/* ************************ IMAGE ICON STARTS ************************ */ array(
				'name'                     => __( 'Trigger', WCST_SLUG ),
				'desc'                     => __( 'You can display random numbers on website to boost your sales.', WCST_SLUG ),
				'id'                       => '_wcst_data_wcst_dynamic_number_mode',
				'type'                     => 'wcst_switch',
				'before_row'               => array( 'WCST_Admin_CMB2_Support', 'cmb_before_row_cb' ),
				'wcst_accordion_title'     => __( 'Dynamic Numbers Box', WCST_SLUG ),
				'wcst_is_accordion_opened' => true,
				'default'                  => 0,
				'label'                    => array(
					'on'  => __( 'Activate', WCST_SLUG ),
					'off' => __( 'Deactivate', WCST_SLUG ),
				),
			),
			array(
				'name'        => __( 'Dynamic Number Range', WCST_SLUG ),
				'id'          => '_wcst_data_wcst_dynamic_number_range_from',
				'type'        => 'text',
				'row_classes' => array( 'wcst_combine_2_field_small_start' ),
				'before'      => __( 'From', WCST_SLUG ),
				'attributes'  => array(
					'placeholder'            => '0',
					'type'                   => 'number',
					'min'                    => '0',
					'pattern'                => '\d*',
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			array(
				'name'        => __( 'To', WCST_SLUG ),
				'id'          => '_wcst_data_wcst_dynamic_number_range_to',
				'type'        => 'text',
				'row_classes' => array( 'wcst_combine_2_field_small_end', 'wcst_hide_label' ),
				'before'      => __( 'To', WCST_SLUG ),
				'attributes'  => array(
					'placeholder'            => '10',
					'type'                   => 'number',
					'min'                    => '0',
					'pattern'                => '\d*',
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			array(
				'id'          => 'wcst_dynamic_number_html_range',
				'type'        => 'wcst_html_content_field',
				'row_classes' => array( 'wcst_clear_both wcst_label_gap cmb2-wcst_mb15' ),
				'content'     => __( 'Dynamic number will auto pick from above range.', WCST_SLUG ),
				'attributes'  => array(
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			array(
				'name'        => __( 'Hold Value For', WCST_SLUG ),
				'desc'        => __( 'Seconds', WCST_SLUG ),
				'id'          => '_wcst_data_wcst_dynamic_number_hold_value',
				'row_classes' => array( 'wcst_field_inline_desc' ),
				'type'        => 'text',
				'attributes'  => array(
					'type'                   => 'number',
					'min'                    => '0',
					'pattern'                => '\d*',
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			array(
				'id'          => 'wcst_dynamic_number_html_hold_val',
				'type'        => 'wcst_html_content_field',
				'row_classes' => array( 'wcst_clear_both wcst_label_gap cmb2-wcst_mb15' ),
				'content'     => __( 'Dynamic number value held for xx seconds using cookies. Leave 0 for auto change on every refresh.', WCST_SLUG ),
				'attributes'  => array(
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			array(
				'name'       => __( 'Hold Value', WCST_SLUG ),
				'id'         => '_wcst_data_wcst_dynamic_number_hold_value_level',
				'type'       => 'radio_inline',
				'options'    => array(
					'website' => __( 'Site Wide', WCST_SLUG ),
					'product' => __( 'Product Wise', WCST_SLUG ),
				),
				'attributes' => array(
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			array(
				'id'          => 'wcst_dynamic_number_html_site_wide',
				'type'        => 'wcst_html_content_field',
				'row_classes' => array( 'wcst_clear_both wcst_label_gap cmb2-wcst_mb15' ),
				'content'     => __( 'Site wide: This holds dynamic number value for complete site like xx people are online or so.<br/>Product wise: This holds dynamic number value specific to the product like xx people have added to cart or so.', WCST_SLUG ),
				'attributes'  => array(
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			/* ************** START REPEATER ************** */ array(
				'id'           => '_wcst_data_wcst_dynamic_number_text',
				'type'         => 'group',
				'before_group' => array( 'WCST_Admin_CMB2_Support', 'cmb2_wcst_before_dynamic_number_call' ),
				'after_group'  => array( 'WCST_Admin_CMB2_Support', 'cmb2_wcst_after_dynamic_number_call' ),
				'repeatable'   => true, // use false if you want non-repeatable group
				'attributes'   => array(
					'class'                  => 'wcst_dynamic_number_group wcst_repeated_group',
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
				'options'      => array(
					'group_title'   => __( 'Text', WCST_SLUG ), // since version 1.1.4, {#} gets replaced by row number
					'add_button'    => __( 'Add Text', WCST_SLUG ),
					'remove_button' => __( 'Remove Text', WCST_SLUG ),
					'sortable'      => true, // true to enable sorting
					'closed'        => true, // true to have the groups closed by default
				),
				'fields'       => array(
					array(
						'name'       => __( 'Text', WCST_SLUG ),
						'desc'       => '<i>{{dynamic_number}}</i> displays dynamic number.',
						'id'         => 'text',
						'type'       => 'textarea',
						'attributes' => array(
							'rows' => '4',
						),
					),
					array(
						'name'    => __( 'Select Condition', WCST_SLUG ),
						'id'      => 'condition',
						'type'    => 'select',
						'options' => array(
							'1' => 'Greater Than',
							'2' => 'Less Than',
						),
					),
					array(
						'name'       => __( 'Number', WCST_SLUG ),
						'id'         => 'number',
						'type'       => 'text',
						'attributes' => array(
							'type'    => 'number',
							'min'     => '0',
							'pattern' => '\d*',
						),
					),
				),
			),
			/* ****************** END REPEATER ****************** */ array(
				'name'       => __( 'Background Color', WCST_SLUG ),
				'id'         => '_wcst_data_wcst_dynamic_number_box_bg_color',
				'type'       => 'colorpicker',
				'attributes' => array(
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			array(
				'name'       => __( 'Border Color', WCST_SLUG ),
				'id'         => '_wcst_data_wcst_dynamic_number_border_color',
				'type'       => 'colorpicker',
				'attributes' => array(
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			array(
				'name'       => __( 'Text Color', WCST_SLUG ),
				'id'         => '_wcst_data_wcst_dynamic_number_text_color',
				'type'       => 'colorpicker',
				'attributes' => array(
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			array(
				'name'       => __( 'Active Number Color', WCST_SLUG ),
				'id'         => '_wcst_data_wcst_dynamic_number_active_color',
				'type'       => 'colorpicker',
				'attributes' => array(
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			array(
				'name'        => __( 'Font Size', WCST_SLUG ),
				'desc'        => __( 'px', WCST_SLUG ),
				'id'          => '_wcst_data_wcst_dynamic_number_font_size',
				'row_classes' => array( 'wcst_field_inline_desc' ),
				'type'        => 'text',
				'attributes'  => array(
					'type'                   => 'number',
					'min'                    => '0',
					'pattern'                => '\d*',
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
			),
			array(
				'name'             => __( 'Position', WCST_SLUG ),
				'id'               => '_wcst_data_wcst_dynamic_number_position',
				'show_option_none' => false,
				'type'             => 'select',
				'after_row'        => array( 'WCST_Admin_CMB2_Support', 'cmb_after_row_cb' ),
				'attributes'       => array(
					'data-conditional-id'    => '_wcst_data_wcst_dynamic_number_mode',
					'data-conditional-value' => '1',
				),
				'options'          => array(
					'1'  => __( 'Above the Title', WCST_SLUG ),
					'2'  => __( 'Below the Title', WCST_SLUG ),
					'3'  => __( 'Below the Review Rating', WCST_SLUG ),
					'4'  => __( 'Below the Price', WCST_SLUG ),
					'5'  => __( 'Below Short Description', WCST_SLUG ),
					'6'  => __( 'Below Add to Cart Button', WCST_SLUG ),
					'7'  => __( 'Below SKU & Categories', WCST_SLUG ),
					'8'  => __( 'Above the Tabs', WCST_SLUG ),
					'11' => __( 'Below Related Products', WCST_SLUG ),
				),
			),
		);
	}

	public function handle_single_product_request( $data, $productInfo, $position ) {

		if ( ! in_array( $productInfo->product_type, $this->get_supported_product_type() ) ) {
			return;
		}
		foreach ( $data as $trigger_key => $dynamic_number_single ) {
			$badge_position = $dynamic_number_single['position'];
			if ( $badge_position == $position ) {
				WCST_Common::insert_log( 'Single Product Request For ' . $productInfo->product->get_id() . '-- ' . $this->get_title(), $this->slug );

				$this->output_html( $trigger_key, $dynamic_number_single, $productInfo, 'product' );
			}
		}
	}

	public function get_supported_product_type() {
		$parent = parent::get_supported_product_type();

		array_push( $parent, 'external' );
		array_push( $parent, 'grouped' );
		array_push( $parent, 'booking' );
		array_push( $parent, 'composite' );

		return $parent;
	}

	public function get_title() {
		return __( 'Dynamic Number', WCST_SLUG );
	}

	public function output_html( $trigger_key, $dynamic_number_single, $productInfo, $page = '', $helper_args = array() ) {
		$classes             = apply_filters( 'wcst_html_classes', '', $this->slug );
		$classes             .= ' wcst_on_' . $page;
		$dynamic_number_html = '';

		if ( isset( $dynamic_number_single['text'] ) && is_array( $dynamic_number_single['text'] ) && count( $dynamic_number_single['text'] ) > 0 ) {
			echo '<div class=" ' . trim( $classes ) . ' wcst_dynamic_number wcst_dynamic_number_key_' . $productInfo->product->get_id() . '_' . $trigger_key . '" data-product-id="' . $productInfo->product->get_id() . '" data-trigger-id="' . $trigger_key . '" data-hold-value-type="' . $dynamic_number_single['hold_value_level'] . '"><div class="xlwcst_spinner"></div></div>';
		}
	}

	public function output_dynamic_css( $data, $productInfo ) {
		$wcst_dynamic_number_arr = $data;
		$dynamic_number_css      = '';
		if ( ! $productInfo->product ) {
			return '';
		}

		foreach ( $wcst_dynamic_number_arr as $trigger_key => $dynamic_number_single ) {
			ob_start();
			$box_bg_color = $dynamic_number_single['box_bg_color'];
			$border_color = $dynamic_number_single['border_color'];
			$text_color   = $dynamic_number_single['text_color'];
			$active_color = $dynamic_number_single['active_color'];
			$font_size    = $dynamic_number_single['font_size'];

			?>
            body .wcst_dynamic_number.wcst_dynamic_number_key_<?php echo $productInfo->product->get_id(); ?>_<?php echo $trigger_key; ?> {
			<?php
			echo $box_bg_color ? ' background: ' . $box_bg_color . ';' : '';
			echo $border_color ? ' border: 1px solid ' . $border_color . ';' : '';
			echo $text_color ? ' color: ' . $text_color . ';' : '';
			echo $font_size ? ' font-size: ' . $font_size . '; line-height: 1.4;' : '';

			echo 'padding: 10px 15px;';
			echo 'margin: 15px 0;';
			?>
            }
            body .wcst_dynamic_number.wcst_dynamic_number_key_<?php echo $productInfo->product->get_id(); ?>_<?php echo $trigger_key; ?> span {
			<?php
			echo $active_color ? ' color: ' . $active_color . ';' : '';
			?>
            }
			<?php

			$dynamic_number_css .= ob_get_clean();
		}

		return $dynamic_number_css;
	}

}

WCST_Triggers::register( new WCST_Trigger_Dynamic_Number() );
